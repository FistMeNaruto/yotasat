# Import the library
from czml import czml

# Initialize a document
doc = czml.CZML()

# Create and append the document packet
packet1 = czml.CZMLPacket(id='document', name="YOTA Lion Sat", version='1.0')
doc.packets.append(packet1)

# Write the CZML document to a file
filename = "example.czml"
doc.write(filename)

import Cesium from "cesium/Cesium";
import "cesium/Widgets/widgets.css";

Cesium.Ion.defaultAccessToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI5MWMwNTZiZS0zZGIxLTQzYTAtYWY3OC03ZTFkZjRmNGM1YWYiLCJpZCI6MjYyOCwiaWF0IjoxNTMzOTA3MDQxfQ.VyXjzgh2RssmgvzOaCfQkFc7xFrmTeU7K1oJfqDicZA";

let viewer = new Cesium.Viewer("cesiumContainer", {
  scene3DOnly: true,
  selectionIndicator: false,
  baseLayerPicker: false,
  geocoder: false,
  homeButton: false,
  // timeline: false,
  // animation: false,
  navigationHelpButton: false
});

// Load Cesium World Terrain
viewer.terrainProvider = Cesium.createWorldTerrain({
  requestWaterMask: true, // required for water effects
  requestVertexNormals: true // required for terrain lighting
});

// Enable depth testing so things behind the terrain disappear.
viewer.scene.globe.depthTestAgainstTerrain = true;
// Enable lighting based on sun/moon positions
viewer.scene.globe.enableLighting = true;

// Create an initial camera view
let initialPosition = new Cesium.Cartesian3.fromDegrees(
  29.22267,
  -26.480756,
  2000
);

let homeCameraView = {
  destination: initialPosition,
  orientation: {
    heading: Cesium.Math.toRadians(240),
    pitch: Cesium.Math.toRadians(-20),
    roll: 0.0
  }
};
// Set the initial view
viewer.scene.camera.setView(homeCameraView);

// Add some camera flight animation options
homeCameraView.duration = 2.0;
homeCameraView.pitchAdjustHeight = 2000;
homeCameraView.endTransform = Cesium.Matrix4.IDENTITY;

// Set up clock and timeline.
viewer.clock.startTime = Cesium.JulianDate.fromIso8601("2018-08-10T06:00:00Z");
viewer.clock.stopTime = Cesium.JulianDate.fromIso8601("2018-08-10T09:00:00Z");
viewer.clock.currentTime = Cesium.JulianDate.fromIso8601(
  "2018-08-10T06:00:00Z"
);

viewer.clock.multiplier = 10; // sets a speedup
viewer.clock.clockStep = Cesium.ClockStep.SYSTEM_CLOCK_MULTIPLIER; // tick computation mode
viewer.timeline.zoomTo(viewer.clock.startTime, viewer.clock.stopTime); // set visible range

import baloonData from "./data/BACARdata.czml";
// Load a baloon flight path from a CZML file
var baloonPromise = Cesium.CzmlDataSource.load(baloonData);
console.log(baloonData);

baloonPromise.then(function(dataSource) {
  viewer.dataSources.add(dataSource);

  var baloon = dataSource.entities.getById("Aircraft/Aircraft1");

  // Smooth path interpolation
  baloon.position.setInterpolationOptions({
    interpolationDegree: 3,
    interpolationAlgorithm: Cesium.HermitePolynomialApproximation
  });
  // Create a follow camera by tracking the baloon entity
  viewer.trackedEntity = baloon;
});
